# coding=utf-8

"""
Package module for RanSaC models.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np


class RanSaCModel:
    """Abstract base class for RanSaC models."""

    # Cardinality is the number of sample points necessary for
    # creating a new model instance.
    cardinality: int = None
    # Storage for lates selection of point mask and indices from model
    # creation
    latest_selection: tuple[np.array, np.array] = None
    
    def select_points(self, ps, tol):
        """Select all points in 'ps' within model distance 'tol'."""
        dists = self.dists(ps)
        mask = dists < tol
        idxs = np.where(mask)[0]
        self.latest_selection = (mask, idxs)
        return mask, idxs

    def dists(self, ps):
        """Return an array of distances between the points in 'ps' and this
        model.
        """
        raise NotImplementedError()


from .line import LineModel
from .line_segment import LineSegmentModel
from .plane import PlaneModel
from .circle_2d import Circle2DModel
from .circle_3d import Circle3DModel
