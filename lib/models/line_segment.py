# coding=utf-8

"""
Line segment model for RanSaC'ing.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import typing

import numpy as np

from . import RanSaCModel
from . import LineModel


class LineSegmentModel(LineModel):
    """A simple line segment model, which piggybacks on the
    LineModel. I.e. it has two points for specifying the model, which
    are only used for specifying the line. After selection of line
    points, Euclidean clustering determines and selects the largest
    segment. This fixes the point at one end, the direction unit
    vector, and a length of the segment along the direction unit
    vector.
    """
    def __init__(self, point, direction):
        LineModel.__init__(self, point, direction)
        self.length = None
        self.e = None

    @classmethod
    def new_random_model(cls,
                         ps: np.ndarray,  # (N,3)-array [m]
                         res_tol: float,  # Residual tolerance for model inliers
                         min_sep: float = 2.0e-3,  # [m]
                         cluster_tol: float = 1.0e-3,  # [m]
                         min_length: float = 10.0e-3, # [m]
                         max_attempts: int = 1000,  # []
                         direction: np.ndarray = None,  # (3,)-array []
                         dir_tol: float = None  # [m]
                         ):
        """Select the required number of points from 'ps' at random for
        fitting a model. 'min_sep' denotes the minimal acceptable
        distance between the two points selected for initializing the
        model. 'min_length' denotes the minimum length of the final
        line segment, chosen as the largest Euclidean
        cluster. 'cluster_tol' denotes the Euclidean cut-off distance
        for points in the same cluster. 'max_attempts' denotes the
        maximum number of attempts at finding an allowable
        model. 'direction', if given, imposes a restriction on the
        line segment direction to be within 'dir_tol' radians from
        'direction'.
        """
        m = None
        nps, dim = ps.shape
        attempts = 0
        while m is None and attempts < max_attempts:
            attempts += 1
            # Find an acceptable set of initial points.
            m = LineModel.new_random_model(
                ps, res_tol, min_sep=min_sep, max_attempts=max_attempts,
                direction=direction, dir_tol=dir_tol)
            if m is None:
                break
            # Convert to LineSegmentModel
            m = cls(m.p, m.ud)
            # Select the points for the line based on the line model metric.
            psl_mask, psl_idxs = LineModel.select_points(m, ps, res_tol)
            psl = ps[psl_idxs]
            # Clustering Project all line points on the line,
            # obtaining the line parameter for the projected points.
            t_ps = (psl - m.p).dot(m.ud)
            # Sort for easy clustering
            t_ps.sort()

            # # Form clusters the hard way iterating all point projections
            # c = [t_ps[0]]
            # cs = [c]
            # for t in t_ps[1:]:
            #     if np.abs(t-c[-1]) <= cluster_tol:
            #         c.append(t)
            #     else:
            #         c = [t]
            #         cs.append(c)

            # Form clusters the fast way by locating gaps in one go on
            # difference array
            gap_idxs = np.where(np.diff(t_ps) >= cluster_tol)[0] + 1
            cs = np.split(t_ps, gap_idxs)
            # Measure cluster lengths to determine winning cluster
            clens = [np.abs(c[-1] - c[0]) for c in cs]
            icmax = np.argmax(clens)
            cmax = cs[icmax]
            lcmax = clens[icmax]
            if lcmax >= min_length:
                m.p = m.p + cmax[0] * m.ud
                m.length = cmax[-1] - cmax[0]
                m.e = m.p + m.length * m.ud
                m.c = m.p + 0.5 * m.length * m.ud
            else:
                # Clear the trial model
                m = None
        return m

    @property
    def geo_object(self):
        """Return the underlying geometry object, either a math2d.LineSegment or a
        math3d.LineSegment, depending on dimension.
        """
        if self.dim == 2:
            import math2d.geometry as m2dg
            return m2dg.LineSegment(self.p, self.e)
        elif self.dim == 3:
            import math3d.geometry as m3dg
            return m3dg.LineSegment(self.p, self.e)

    def dists(self, ps):
        """Calculate the distance to all points."""
        # Check if this segment has a length.
        if self.length is None:
            return LineModel.dists(self, ps)
        # Calculate the projections for checking if line segment is exceeeded
        ts = (ps - self.p).dot(self.ud)
        # Sample the dists
        dists = []
        for t, p in zip(ts, ps):
            if t < 0:
                # Out of line segment at lower end
                dists.append(np.linalg.norm(self.p - p))
            elif t > self.length:
                # Out of line segment at further end
                dists.append(np.linalg.norm(self.e - p))
            else:
                # Projection falls on the line segment. Calculate
                # distance to projected point.
                dists.append(np.linalg.norm(self.p + t*self.ud - p))
        return np.array(dists)
