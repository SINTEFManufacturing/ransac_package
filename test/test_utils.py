# coding=utf-8

"""
Test script for 2D circle RanSaC'ing.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time

import numpy as np
import math2d as m2d
import math3d as m3d

from ransac.models import Circle2DModel, Circle3DModel, RanSaCModel
from ransac import multi_ransac


def generate_circle_test_set(models: list[RanSaCModel],
                             n_points=1000, model_frac=0.2, gauss_noise=0.01):
    mtypes = list({type(m) for m in models})
    if len(mtypes) != 1:
        raise Exception(f'generate_circle_test_set: Different model types present: {mtypes}')
    elif mtypes[0] not in (Circle2DModel, Circle3DModel):
        raise Exception(f'generate_circle_test_set: Models given are circle models: {mtypes[0]}')
    mtype = mtypes[0]
    dim = 3 if mtype == Circle3DModel else 2
    r_max = max([m.radius for m in models])
    x_range = [min([m.centre[0] for m in models]) -
               2*r_max, max([m.centre[0] for m in models]) + 2*r_max]
    y_range = [min([m.centre[1] for m in models]) -
               2*r_max, max([m.centre[1] for m in models]) + 2*r_max]
    ranges = np.array([x_range, y_range])
    rps = np.random.uniform(ranges.T[0], ranges.T[1], (n_points, dim))
    nm = int(n_points * model_frac)
    circles = []
    for m in models:
        angles = np.random.uniform(0, 2*np.pi, nm)
        radii = m.radius * np.array([np.cos(angles), np.sin(angles)]).T
        mpts = m.centre + radii + np.random.normal(0, gauss_noise, (nm, dim))
        circles.append((m, mpts))
    mpoints_tot = np.vstack([c[1] for c in circles])
    ps = np.vstack((rps, mpoints_tot))
    return ps, rps, mpoints_tot, circles
