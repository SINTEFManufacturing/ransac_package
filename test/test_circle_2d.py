# coding=utf-8

"""
Test script for 2D circle RanSaC'ing.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2019"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time

import numpy as np
import math2d as m2d
import math3d as m3d

from ransac.models import Circle2DModel, RanSaCModel
from ransac import multi_ransac

from test_utils import generate_circle_test_set


def test_circle2d_model(plot=True):
    global rps, ps, cm, c0s, ems
    a = np.pi/3
    sa = np.sin(a)
    ca = np.cos(a)
    c0 = np.array([1.1, 2.2])
    r0 = 1.0
    ps = np.array([[ca, sa], [0, 1], [-ca, sa]]) + c0
    cm = Circle2DModel.new_fitted_points(ps)
    assert(np.allclose(cm.centre.T, c0))
    assert(np.allclose(cm.radius, r0))
    rps = ps + np.random.normal(0.0, 0.01, (3, 2))
    # print(cm.select_points(rps, 0.01))
    n_points = 500
    models = [Circle2DModel([1, 2], 1.0),
              Circle2DModel([-1, 1], 2.0),
              Circle2DModel([1, -1], 0.5)]
    ps, rps, m0ps, c0s = generate_circle_test_set(
        models,
        n_points, model_frac=0.2,
        gauss_noise=0.05)
    t0 = time.time()
    ems, rem_ps = multi_ransac(ps, n_models=len(models),
                               model_type=Circle2DModel,
                               model_kwargs=dict(min_radius=0.1,
                                                 max_radius=3.0, min_sep=0.01),
                               n_iter=1000, res_tol=0.05, cons_tol=0.1,
                               regress=False, max_unimp_count=100)
    print('Time for ransac\'ing:', time.time() - t0)
    print('Number of fitted points:', [em.ins.shape[0] for em in ems])
    if plot:
        import matplotlib.pyplot as plt
        fig = plt.figure()
        proj = 'rectilinear'
        ax = fig.add_subplot(111, projection=proj, aspect='equal')
        ax.scatter(*ps.T, s=5, c='g', marker='o')
        ax.scatter(*m0ps.T, s=10, c='b', marker='o')
        for em in ems:
            circ = plt.Circle(em[0].centre, em[0].radius,
                              color='r', fill=False)
            ax.add_artist(circ)
            ax.scatter(*em.cons.T, s=5, c='r', marker='x')
        plt.show(block=False)
    return ems


if __name__ == '__main__':
    e_models = test_circle2d_model()
